#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <speak.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<Speak>("QtSpeak", 1, 0, "Speak");

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("version", QString(GIT_VERSION));

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
